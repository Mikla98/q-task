@extends('layout')

@section('content')

    <div class="flex items-center w-full">

        <div class="flex flex-col">

         <p class="text-2xl text-gray-800 font-bold">{{$author['first_name']}} {{$author['last_name']}}</p> 
         

         <p class="text-lg text-gray-600 font-light mt-4">{{$author['biography']}}</p>    

         
         

        @if (count($author['books']) == 0)
            
        <form action="{{route('author.destroy', $author['id'])}}" method="POST">

            @csrf
            @method('DELETE') 

            <button type="submit" class="text-red-600 hover:text-red-900">Delete Author</button>

        </form>

        @endif
         


        </div>

    </div>

    <!-- This example requires Tailwind CSS v2.0+ -->
<div class="flex flex-col mt-10">
    <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
      <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
        <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
          <table class="min-w-full divide-y divide-gray-200">
            <thead class="bg-gray-50">
              <tr>
                <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                  Title
                </th>
                <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                  ISBN
                </th>
                <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                  Format
                </th>
                <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                  Release date
                </th>
                <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                  Number of pages
                </th>
                <th scope="col" class="relative px-6 py-3">
                  <span class="sr-only">Delete</span>
                </th>
              </tr>
            </thead>
            <tbody class="bg-white divide-y divide-gray-200">

                @foreach ($author['books'] as $book)

                <tr>
                    <td class="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                      {{$book['title']}}
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                      {{$book['isbn']}}
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                      {{$book['format']}}
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                      {{$book['release_date']}}
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                      {{$book['number_of_pages']}}
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                        <form action="{{route('book.destroy', $book['id'])}}" method="POST">

                            @csrf
                            @method('DELETE') 

                            <button type="submit" class="text-red-600 hover:text-red-900">Delete</button>

                        </form>
                      
                    </td>
                </tr>
                    
                @endforeach

  
              <!-- More people... -->
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  
    
@endsection