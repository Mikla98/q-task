@extends('layout')

@section('content')

<div class="min-h-full flex flex-col justify-center py-12 sm:px-6 lg:px-8">
    <div class="sm:mx-auto sm:w-full sm:max-w-md">
    <h2 class="mt-6 text-center text-3xl font-extrabold text-gray-900">
        Dodaj knjigu
    </h2>
    </div>

    <div class="mt-8 sm:mx-auto sm:w-full sm:max-w-md">
    <div class="bg-white py-8 px-4 shadow sm:rounded-lg sm:px-10">
        <form action="{{route('book.store')}}" class="space-y-6" method="POST">
        
        @csrf

        <div>
            <label for="title" class="block text-sm font-medium text-gray-700">
            Title
            </label>
            <div class="mt-1">
            <input id="title" name="title" type="text" autocomplete="title" required class="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
            </div>
        </div>



        <div>
            <label for="description" class="block text-sm font-medium text-gray-700">
            Description
            </label>
            <div class="mt-1">
            <input id="description" name="description" type="text" autocomplete="description" required class="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
            </div>
        </div>

        <div>
            <label for="isbn" class="block text-sm font-medium text-gray-700">
            ISBN
            </label>
            <div class="mt-1">
            <input id="isbn" name="isbn" type="text" autocomplete="isbn" required class="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
            </div>
        </div>

        <div>
            <label for="format" class="block text-sm font-medium text-gray-700">
            Format
            </label>
            <div class="mt-1">
            <input id="format" name="format" type="text" autocomplete="format" required class="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
            </div>
        </div>

        <div>
            <label for="number_of_pages" class="block text-sm font-medium text-gray-700">
            Number of pages
            </label>
            <div class="mt-1">
            <input id="number_of_pages" name="number_of_pages" type="number" autocomplete="number_of_pages" required class="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
            </div>
        </div>


        <div>
            <label for="authorId" class="block text-sm font-medium text-gray-700">Location</label>
            <select id="authorId" name="authorId" class="mt-1 block w-full pl-3 pr-10 py-2 text-base border-gray-300 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm rounded-md">
                @foreach ($authors as $author)
                <option value="{{$author['id']}}">{{$author['first_name']}} {{$author['last_name']}}</option>
                @endforeach
            </select>
        </div>
  


        <div>
            <button type="submit" class="w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
            Dodaj
            </button>
        </div>
        </form>

    </div>
    </div>
</div>
    
@endsection