@extends('layout')

@section('content')

<div class="min-h-full flex flex-col justify-center py-12 sm:px-6 lg:px-8">
    <div class="sm:mx-auto sm:w-full sm:max-w-md">
    <h2 class="mt-6 text-center text-3xl font-extrabold text-gray-900">
        Add author
    </h2>
    </div>

    <div class="mt-8 sm:mx-auto sm:w-full sm:max-w-md">
    <div class="bg-white py-8 px-4 shadow sm:rounded-lg sm:px-10">
        <form action="{{route('author.store')}}" class="space-y-6" method="POST">
        
        @csrf

        <div>
            <label for="first_name" class="block text-sm font-medium text-gray-700">
            First Name
            </label>
            <div class="mt-1">
            <input id="first_name" name="first_name" type="text" autocomplete="first_name" required class="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
            </div>
        </div>



        <div>
            <label for="last_name" class="block text-sm font-medium text-gray-700">
            Last Name
            </label>
            <div class="mt-1">
            <input id="last_name" name="last_name" type="text" autocomplete="last_name" required class="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
            </div>
        </div>

        <div>
            <label for="birthday" class="block text-sm font-medium text-gray-700">
            Birthday
            </label>
            <div class="mt-1">
            <input id="birthday" name="birthday" type="date" autocomplete="birthday" required class="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
            </div>
        </div>

        <div>
            <label for="biography" class="block text-sm font-medium text-gray-700">
            Biography
            </label>
            <div class="mt-1">
            <input id="biography" name="biography" type="text" autocomplete="biography" required class="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
            </div>
        </div>

        <div>
            <label for="birth" class="block text-sm font-medium text-gray-700">
            Place of Birth
            </label>
            <div class="mt-1">
            <input id="birth" name="birth" type="text" autocomplete="birth" required class="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
            </div>
        </div>


        <div>
            <label for="gender" class="block text-sm font-medium text-gray-700">Gender</label>
            <select id="gender" name="gender" class="mt-1 block w-full pl-3 pr-10 py-2 text-base border-gray-300 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm rounded-md">

                <option value="male">Male</option>
                <option value="female">Female</option>

            </select>
        </div>
  


        <div>
            <button type="submit" class="w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
            Dodaj
            </button>
        </div>
        </form>

    </div>
    </div>
</div>
    
@endsection