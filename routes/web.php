<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\AuthorController;
use App\Http\Controllers\BookController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
})->name('welcome');

Route::post('/login', [LoginController::class, 'login'])
                ->name('login');

Route::get('/logout', [LoginController::class, 'logout'])
                ->name('logout')->middleware('isLogged');
                
Route::get('/authors/new', [AuthorController::class, 'create'])
                ->name('author.create')->middleware('isLogged');

Route::post('/authors/store', [AuthorController::class, 'store'])
                ->name('author.store')->middleware('isLogged');

Route::get('/authors', [AuthorController::class, 'allAuthors'])
                ->name('authors')->middleware('isLogged');

Route::get('/authors/{id}', [AuthorController::class, 'author'])
                ->name('author')->middleware('isLogged');


Route::delete('/authors/{id}', [AuthorController::class, 'destroy'])
                ->name('author.destroy')->middleware('isLogged');

Route::delete('/books/{id}', [BookController::class, 'destroy'])
                ->name('book.destroy')->middleware('isLogged');

Route::get('/books/new', [BookController::class, 'create'])
                ->name('book.create')->middleware('isLogged');

Route::post('/books/store', [BookController::class, 'store'])
                ->name('book.store')->middleware('isLogged');
