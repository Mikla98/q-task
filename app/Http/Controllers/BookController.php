<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Carbon\Carbon;
use App\Repositories\AuthorRepository;
use App\Repositories\BookRepository;
use App\Http\Requests\NewBookRequest;

class BookController extends Controller
{

    public function destroy(BookRepository $bookRepository,$id){

            $bookRepository->destroyBook($id);

            return redirect()->route('authors');

    }


    public function create(AuthorRepository $authorRepository){

            $authors = $authorRepository->getAllAuthors()->json()['items'];

            return view('pages.addbook')->with('authors',$authors);


    }

    public function store(BookRepository $bookRepository,NewBookRequest $request){

        $authorId = $request->authorId;
        $title = $request->title;
        $isbn = $request->isbn;
        $releaseDate = Carbon::now()->toDateTimeString();
        $format = $request->format;
        $number_of_pages = $request->number_of_pages;

        dd($bookRepository->createBook($authorId,$title,$releaseDate,$isbn,$format,$number_of_pages));

        return redirect()->route('authors');
    }
    
}
