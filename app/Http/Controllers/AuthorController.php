<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\AuthorRepository;
use App\Http\Requests\NewAuthorRequest;

class AuthorController extends Controller
{
    public function allAuthors(AuthorRepository $authorRepository){


        $authors = $authorRepository->getAllAuthors()->json()['items'];
    
        return view('pages.authors')->with('authors',$authors);
            

    }

    public function author(AuthorRepository $authorRepository,$id){ 

        $author = $authorRepository->getAuthor($id)->json();
        
        return view('pages.author')->with('author',$author);


    }

    public function destroy(AuthorRepository $authorRepository,$id){

        $authorRepository->destroyAuthor($id);

        return redirect()->route('authors');

    }

    public function create(){

        return view('pages.addauthor');


    }

    public function store(AuthorRepository $authorRepository,NewAuthorRequest $request){

        $firstname = $request->first_name;
        $lastname = $request->last_name;
        $birth = $request->birthday;
        $bio = $request->biography;
        $gender = $request->gender;
        $placebirth = $request->birth;

        $authorRepository->createAuthor($firstname,$lastname,$birth,$bio,$gender,$placebirth);

        return redirect()->route('authors');
    }
}
