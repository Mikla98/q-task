<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;
use App\Repositories\LoginRepository;
use App\Http\Requests\LoginRequest;

class LoginController extends Controller
{
    public function login(LoginRepository $loginRepository,LoginRequest $request){

        $email = $request->email;
        $password = $request->password;

        $response = $loginRepository->loginUser($email,$password);

        session(['token' => $response['token_key']]);
        session(['first_name' => $response['user']['first_name']]);
        session(['last_name' => $response['user']['last_name']]);

            
        return redirect()->route('authors');
                                                                                                                                                                                                                
    }

    public function logout(){

            session()->flush();

            return redirect()->route('welcome');

    }
}
