<?php

namespace App\Repositories;

use Illuminate\Support\Facades\Http;

class BookRepository
{

    public function destroyBook($id){

        $response = Http::withToken(session('token'))->delete('https://symfony-skeleton.q-tests.com/api/v2/books/'.$id);

        return $response;

    }

    public function createBook($id,$title,$release,$isbn,$format,$nop){
        
        $response = Http::withToken(session('token'))->withHeaders([
            'accept' => 'application/json',
            'Content-Type' => 'application/json',
            ])->post('https://symfony-skeleton.q-tests.com/api/v2/books', [

                'author' => [
                    'id' =>  $id
                ],
                'title' => $title,
                'release_date' => $release,
                'isbn' => $isbn,
                'format' => $format,
                'number_of_pages' => $nop
        ]);

        return $response;

    }


}