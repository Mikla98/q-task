<?php

namespace App\Repositories;

use Illuminate\Support\Facades\Http;

class AuthorRepository
{

    public function getAllAuthors(){

        $response = Http::withToken(session('token'))->withHeaders([
            'accept' => 'application/json',
            'Content-Type' => 'application/json'
            
            ])->get('https://symfony-skeleton.q-tests.com/api/v2/authors?orderBy=id&direction=ASC&limit=100&page=1');

        return $response;
    }

    public function getAuthor($id){

        $response = Http::withToken(session('token'))->withHeaders([
            'accept' => 'application/json',
            'Content-Type' => 'application/json'
            
            ])->get('https://symfony-skeleton.q-tests.com/api/v2/authors/'.$id);

        return $response;


    }

    public function destroyAuthor($id){

        $response = Http::withToken(session('token'))->delete('https://symfony-skeleton.q-tests.com/api/v2/authors/'.$id);

        return $response;

    }

    public function createAuthor($firstname,$lastname,$birth,$bio,$gender,$placebirth){

        $response = Http::withToken(session('token'))->withHeaders([
            'accept' => 'application/json',
            'Content-Type' => 'application/json',
            ])->post('https://symfony-skeleton.q-tests.com/api/v2/authors', [

                'first_name' => $firstname,
                'last_name' => $lastname,
                'birthday' => $birth,
                'biography' => $bio,
                'gender' => $gender,
                'place_of_birth' => $placebirth
        ]);

        return $response;
    }
}