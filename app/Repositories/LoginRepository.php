<?php

namespace App\Repositories;

use Illuminate\Support\Facades\Http;

class LoginRepository
{

public function loginUser($email, $password){

    $response = Http::withHeaders([
        'accept' => 'application/json',
        'Content-Type' => 'application/json'
        ])->post('https://symfony-skeleton.q-tests.com/api/v2/token', [

        'email' => $email,
        'password' => $password
        ]);

    return $response;

}


}